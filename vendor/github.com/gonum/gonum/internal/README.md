# Gonum internal [![GoDoc](https://godoc.org/github.com/gonum/gonum/internal?status.svg)](https://godoc.org/github.com/gonum/gonum/internal)

This is the set of internal packages for the Gonum project.
