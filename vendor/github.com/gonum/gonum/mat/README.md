# Gonum matrix [![GoDoc](https://godoc.org/github.com/gonum/gonum/matrix?status.svg)](https://godoc.org/github.com/gonum/gonum/matrix)

Package matrix is a matrix package for the Go language.
