// Copyright ©2017 The gonum Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// package amos implements functions originally in the Netlab code by Donald Amos.
package amos // import "github.com/gonum/gonum/mathext/internal/amos"
