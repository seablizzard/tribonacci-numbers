# mathext [![GoDoc](https://godoc.org/github.com/gonum/gonum/mathext?status.svg)](https://godoc.org/github.com/gonum/gonum/mathext)

Package mathext implements basic elementary functions not included in the Go standard library.
