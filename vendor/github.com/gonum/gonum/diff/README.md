# Gonum diff [![GoDoc](https://godoc.org/github.com/gonum/gonum/diff?status.svg)](https://godoc.org/github.com/gonum/gonum/diff)

Package diff is a package for computing derivatives of functions for the Go language.
