#!/bin/bash

go generate github.com/gonum/gonum/blas
go generate github.com/gonum/gonum/blas/gonum
go generate github.com/gonum/gonum/unit
go generate github.com/gonum/gonum/graph/formats/dot
if [ -n "$(git diff)" ]; then
	exit 1
fi
