# Gonum optimize [![GoDoc](https://godoc.org/github.com/gonum/gonum/optimize?status.svg)](https://godoc.org/github.com/gonum/gonum/optimize)

Package optimize is an optimization package for the Go language.
