// Copyright ©2017 The gonum Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package blas64 provides a simple interface to the float64 BLAS API.
package blas64 // import "github.com/gonum/gonum/blas/blas64"
