# Gonum graph [![GoDoc](https://godoc.org/github.com/gonum/gonum/graph?status.svg)](https://godoc.org/github.com/gonum/gonum/graph)

This is a generalized graph package for the Go language.
