package main

import (
	"bytes"
	"encoding/json"
	"github.com/Sirupsen/logrus"
	"github.com/takama/router"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
)

// buffer is a special variable to save log messages
var buffer bytes.Buffer

type tribonacciValueByPosition struct {
	position      uint64
	expectedValue float64
}

type responseFormat struct {
	Position uint64  `json:"position"`
	Value    float64 `json:"value"`
	Error    string  `json:"error"`
}

func init() {
	log.Out = &buffer
	log.Formatter = &logrus.JSONFormatter{}
}

// TestLogger checks if logger handler works correctly
func TestLogger(t *testing.T) {
	r := router.New()
	r.Logger = logger

	testedServer := httptest.NewServer(r)
	defer testedServer.Close()

	_, err := http.Get(testedServer.URL + "/")
	if err != nil {
		t.Fatal(err)
	}

	logFormat := struct {
		Level string `json:"level"`
		Msg   string `json:"msg"`
		Time  string `json:"time"`
	}{}
	err = json.NewDecoder(&buffer).Decode(&logFormat)
	if err != nil {
		t.Fatal(err)
	}

	msgParts := strings.Split(logFormat.Msg, " ")
	if len(msgParts) != 3 {
		t.Fatalf("Wrong message was logged: %s", logFormat.Msg)
	}
}

// TestHandlerTribonacci checks if the handler produces proper tribonacci values and errors for different cases
func TestHandlerTribonacci(t *testing.T) {
	r := router.New()
	r.GET("/api/v1/tribonacci/:position", tribonacci)

	ts := httptest.NewServer(r)
	defer ts.Close()

	testCases := []tribonacciValueByPosition{
		{0, 0}, {1, 0}, {2, 0}, {3, 1}, {4, 1}, {5, 2}, {10, 44}, {14, 504}, {19, 10609}, {1169, 0},
	}

	for _, testCase := range testCases {
		response, err := http.Get(ts.URL + "/api/v1/tribonacci/" + strconv.FormatUint(testCase.position, 10))
		if err != nil {
			t.Fatal(err)
		}

		assertForNumericPosition(t, response, testCase)
		response.Body.Close()
	}

	assertErrorForNonNumericPosition(t, ts)
}

func assertForNumericPosition(t *testing.T, response *http.Response, testCase tribonacciValueByPosition) {
	rFormat := responseFormat{}
	err := json.NewDecoder(response.Body).Decode(&rFormat)
	if err != nil {
		t.Fatal(err)
	}

	switch {
	case testCase.position != rFormat.Position:
		t.Fatalf(
			"Wrong position in the response. Given '%d', expectedValue '%d'",
			rFormat.Position, testCase.position,
		)
	case testCase.position == 0 && rFormat.Error != "position should be greater than 0":
		t.Fatal("Expected another error message text for the position = 0")
	case testCase.position == 1169 &&
		rFormat.Error != "position should be less than 1169, due to software limitations (x64 processor grid)":
		t.Fatal("Expected another error message text for the position >= 1169")
	case testCase.position > 0 && testCase.position < 1169 && len(rFormat.Error) > 0:
		t.Fatalf("Error message given for the position > 0 (%s)", rFormat.Error)
	case testCase.expectedValue != rFormat.Value:
		t.Fatalf(
			"Wrong tribonacci value in the response. Given '%e', expectedValue '%e'",
			rFormat.Value, testCase.expectedValue,
		)
	}
}

func assertErrorForNonNumericPosition(t *testing.T, ts *httptest.Server) {
	position := "strangePosition"
	response, err := http.Get(ts.URL + "/api/v1/tribonacci/" + position)
	if err != nil {
		t.Fatal(err)
	}

	defer response.Body.Close()

	rFormat := responseFormat{}
	err = json.NewDecoder(response.Body).Decode(&rFormat)
	if err != nil {
		t.Fatal(err)
	}

	if rFormat.Error != "\""+position+"\" looks like not a number" {
		t.Fatal("Wrong error description for non numeric value in the response")
	}
}
