# tribonacci-numbers #

An example of a service, which evaluates a Tribonacci number
 for some N from the request (N is natural number greater than 0).

### What are the Tribonacci numbers? ###

This sequence of numbers is an "expansion" of the fibonacci sequence
 that includes a third term on the sum of the previous two, 
 such that the formula looks like:

```
F(n+3) = F(n+2) + F(n+1) + F(n),  F(0) = 0; F(1) = 0; F(1) = 1; F(2) = 1; F(3) = 2
```
So, the more complete example of the Tribonacci sequense is 
`0, 0, 1, 1, 2, 4, 7, 13, 24, 44, 81, 149, 274, 504, 927, 1705, 3136, 5768, 10609, ...`

### The algorithm description ###
The first way to compute the tribonacci numbers is _recursion_.
Pros: a little code block, simplicity.
Cons: exponential execution time, too slow for big n, call stack overflow.

The second way is _recursion with the storage of intermediate values_. 
Pros: linear execution time.
Cons: uses much more memory, call stack overflow.

The third way is _dynamic programing for two last elements before n_.
Pros: a little code block, simplicity.
Cons: will always be very slow for n as large as 10^15.

Therefore, _Matrix exponentiation algorithm_ is used in the current service. 
 
The basic idea behind matrix exponentiation is to use 
the base cases of the recurrence relationship in order 
to assemble a matrix which will allow us 
to compute values fast.

![Tribonacci base matrix image](MatrixAlgorithmDescription.gif)

Now, to calculate a large Tribonacci number, 
we just apply the matrix multiplication n times, 
and we get: _F(n) = Mt * F(n-1) = Mt^n * F(0)_,
 where _Mt^n_ is the matrix to the power of n.

The power of the matrix can now be computed 
 using repeated squaring applied to matrices.
 
### How to run the service ###
After successful cloning this repo to your machine,
 you should install all service vendors: 
    
    glide install
    
Then you need to revert some changes in `vendors/` directory,
 because `glide` manager deletes useful local copy
 of matrix evaluation library `gonum/gonum/mat`:
     
     git checkout -- ./
 Yes, it look like "dirty trick", but I haven\`t any idea,
 how to save the vendor library, which was stored
  locally because, it has broken include paths 
  in it\`s maintainer repo on Github.

The next step is the service app building from the sources:
    
    env CGO_ENABLED=0 GOOS=linux go build
 
 and make a container with the binary:
 
     docker build -t tribonacci-numbers -f ./Dockerfile .
     
 Finally, you should run service in Docker
 
     docker run -p 8000:8000 tribonacci-numbers 

### Service routes ###
_http://localhost:8000/api/v1/tribonacci/:position/_ - the main
route, where `:position` is uint number.

_http://localhost:8000/healthStatus_ - 
simple health check of the service state (running/stopped).