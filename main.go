package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/k8s-community/utils/shutdown"
	"github.com/takama/router"
	"net/http"
	"os"
)

var log = logrus.New()

// Run server: `go build; env SERVICE_PORT=8000 ./tribonacci-numbers`.
// Try requests: `curl http://127.0.0.1:8000/api/v1/tribonacci/:position`, where `:position` is uint number.
func main() {
	servicePort := os.Getenv("SERVICE_PORT")
	if len(servicePort) == 0 {
		log.Fatal("Required parameter 'SERVICE_PORT' isn`t set")
	}

	appRouter := router.New()
	appRouter.Logger = logger

	appRouter.GET("/api/v1/tribonacci/:position", tribonacci)

	appRouter.GET("/healthStatus", func(c *router.Control) {
		c.Code(http.StatusOK).Body(http.StatusText(http.StatusOK))
	})

	// goroutine is used, because the sdHandler blocks the function execution (waits interrupt signals)
	go shutDownProcess()

	appRouter.Listen("0.0.0.0:" + servicePort)
}

func shutDownProcess() {
	loggerSh := log.WithField("event", "shutdown")
	sdHandler := shutdown.NewHandler(loggerSh)
	sdHandler.RegisterShutdown(shutDown)
}

// shutDown does graceful shutdown of the service
func shutDown() (string, error) {
	return "Ok", nil
}
