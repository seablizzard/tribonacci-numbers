FROM scratch

ENV SERVICE_PORT 8000

EXPOSE $SERVICE_PORT

COPY tribonacci-numbers /

CMD ["/tribonacci-numbers"]
