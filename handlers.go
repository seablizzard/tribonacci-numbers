package main

import (
	"fmt"
	"github.com/gonum/gonum/mat"
	"github.com/takama/router"
	"strconv"
)

// logger provides a log messages, based on the parameters of clients requests
func logger(routerControl *router.Control) {
	remoteAddr := routerControl.Request.Header.Get("X-Forwarded-For")
	if remoteAddr == "" {
		remoteAddr = routerControl.Request.RemoteAddr
	}

	log.Infof("%s %s %s", remoteAddr, routerControl.Request.Method, routerControl.Request.URL.Path)
}

// tribonacci handler provides a response with tribonacci value for the position, given by the request
func tribonacci(routerControl *router.Control) {
	positionFromRequest := routerControl.Get(":position")
	position, err := strconv.ParseUint(positionFromRequest, 10, 64)
	tribonacciValue := float64(0)
	if err == nil {
		tribonacciValue, err = getTribonacciValueByPosition(position)
	} else {
		err = fmt.Errorf("%q looks like not a number", positionFromRequest)
	}

	if err == nil {
		err = fmt.Errorf("")
	}

	type ResponseFields map[string]interface{}
	responseFields := ResponseFields{
		"position": position,
		"value":    tribonacciValue,
		"error":    err.Error(),
	}
	routerControl.Code(200).Body(responseFields)
}

// getTribonacciValueByPosition evaluates the element from the tribonacci sequence, selected by position.
// The evaluation is based on an exponential matrix, in contrast to recursion,
// because it works more efficiently especially on large numbers (see details in README.md).
func getTribonacciValueByPosition(position uint64) (float64, error) {
	switch {
	case position > 1168:
		return 0, fmt.Errorf("position should be less than 1169, due to software limitations (x64 processor grid)")
	case position <= 0:
		return 0, fmt.Errorf("position should be greater than 0")
	case position < 3:
		return 0, nil
	case position < 5:
		return 1, nil
	}

	tribonacciBaseMatrix := mat.NewDense(3, 3, []float64{1, 1, 0, 1, 0, 1, 1, 0, 0})
	tribonacciBaseMatrix.Pow(tribonacciBaseMatrix, int(position)-3)

	return tribonacciBaseMatrix.At(0, 0), nil
}
